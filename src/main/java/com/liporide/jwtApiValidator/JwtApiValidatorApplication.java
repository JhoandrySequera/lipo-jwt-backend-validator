package com.liporide.jwtApiValidator;


import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.liporide.jwtApiValidator.model.AuthorizerResponse;
import com.liporide.jwtApiValidator.model.PolicyDocument;
import com.liporide.jwtApiValidator.model.Statement;
import com.liporide.jwtApiValidator.model.TokenAuthorizerContext;
import com.liporide.jwtApiValidator.util.FirebaseConection;
import com.liporide.jwtApiValidator.util.JWTOptions;

import java.util.Collections;
import java.util.logging.Logger;

public class JwtApiValidatorApplication implements RequestHandler<TokenAuthorizerContext, AuthorizerResponse> {

	private final static Logger LOGGER = Logger.getLogger(JwtApiValidatorApplication.class.getName());
	private static final String PROD_ENVIRONMENT = "production";

	@Override
	public AuthorizerResponse handleRequest(TokenAuthorizerContext request, Context context) {
		Statement statement = getStatement(request.getMethodArn(), "Deny");
		String authorization = request.getAuthorizationToken();
		String firebaseProject = FirebaseConection.getFirebaseProjectName(request.getMethodArn());

		try {
			JWTOptions.validate(firebaseProject,
					authorization.replace("Bearer ", ""),
					isProductionEnvironment(request.getMethodArn()));
			statement = getStatement(request.getMethodArn(), "Allow");
		} catch (Exception e) {
			LOGGER.warning("ERROR validating JWT -> " + e.getMessage());
		}

		return AuthorizerResponse.builder()
				.principalId(firebaseProject)
				.policyDocument(
						PolicyDocument.builder()
								.statements(Collections.singletonList(statement))
								.build())
				.build();
	}

	private boolean isProductionEnvironment(String methodArn) {
		return methodArn.contains(PROD_ENVIRONMENT);
	}

	private Statement getStatement(String arn, String effect) {
		return Statement.builder()
				.resource(arn)
				.effect(effect)
				.build();
	}
}
