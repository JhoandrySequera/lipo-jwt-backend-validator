package com.liporide.jwtApiValidator.util;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.liporide.jwtApiValidator.util.firebase.DriverConnection;
import com.liporide.jwtApiValidator.util.firebase.PassengerConnection;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

public class FirebaseConection {

    private final static Logger LOGGER = Logger.getLogger(FirebaseConection.class.getName());

    private static final String PASSENGER_FIREBASE_PROJECT  = "passenger";
    private static final String DRIVER_FIREBASE_PROJECT = "driver";

    public static String getFirebaseProjectName(String parameter) {
        return  parameter.contains(DRIVER_FIREBASE_PROJECT)? DRIVER_FIREBASE_PROJECT :
                (parameter.contains(PASSENGER_FIREBASE_PROJECT)? PASSENGER_FIREBASE_PROJECT : null);
    }

    public static FirebaseAuth getAuthReference(String nameFirebaseProject, boolean isProductionEnvironment) throws IOException {
        if(PASSENGER_FIREBASE_PROJECT.equalsIgnoreCase(nameFirebaseProject)) {
            PassengerConnection.getAuthReference(isProductionEnvironment);
        }
        if(DRIVER_FIREBASE_PROJECT.equalsIgnoreCase(nameFirebaseProject)) {
            DriverConnection.getAuthReference(isProductionEnvironment);
        }

        throw new RuntimeException("Backend cannot load configuration by driver app");
    }

    public static FirebaseApp loadConfiguration(String credencials, String databaseUrl, String name) throws IOException {
        if(FirebaseApp.getApps().isEmpty() && !isAlreadyRegisteredApp(name)) {
            InputStream inputStream = new ByteArrayInputStream(credencials.getBytes());
            FirebaseOptions firebaseOptions = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(inputStream))
                    .setDatabaseUrl(databaseUrl).build();

            return FirebaseApp.initializeApp(firebaseOptions, name);
        }

        return FirebaseApp.getInstance(name);
    }

    private static boolean isAlreadyRegisteredApp(String name){
        return FirebaseApp.getApps().stream().anyMatch(app -> app.getName().equalsIgnoreCase(name));
    }
}
