package com.liporide.jwtApiValidator.util;

import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public class JWTOptions {

    private Optional<Map<String, Object>> validateAndGetClaims(String firebaseProject, String jwtToken, boolean isProductionEnvironment) throws FirebaseAuthException, IOException {
        FirebaseToken decodedToken = FirebaseConection
                .getAuthReference(firebaseProject, isProductionEnvironment)
                .verifyIdToken(jwtToken);
        return Optional.of(decodedToken.getClaims());
    }

    public static void validate(String firebaseProject, String jwtToken, boolean isProductionEnvironment) throws FirebaseAuthException, IOException {
        FirebaseConection.getAuthReference(firebaseProject, isProductionEnvironment).verifyIdToken(jwtToken);
    }
}
