package com.liporide.jwtApiValidator.util.firebase;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.liporide.jwtApiValidator.util.FirebaseConection;

import java.io.IOException;
import java.util.logging.Logger;

public class DriverConnection extends FirebaseConection {

    private final static Logger LOGGER = Logger.getLogger(DriverConnection.class.getName());

    private static final String DRIVER_FIREBASE_PROJECT = "driver";

    public static FirebaseAuth getAuthReference(boolean isProductionEnvironment) throws IOException {
        LOGGER.info("info by project "+DRIVER_FIREBASE_PROJECT);
        FirebaseApp driverFirebaseApp = loadConfiguration(getCredentials(isProductionEnvironment),
                getrDatabaseUrl(isProductionEnvironment), DRIVER_FIREBASE_PROJECT);
        return FirebaseAuth.getInstance(driverFirebaseApp);
    }

    private static String getrDatabaseUrl(boolean isProductionEnvironment) {
        return isProductionEnvironment? getProductionDataBaseUrl() : getDevelopDataBaseUrl();

    }
    private static String getCredentials(boolean isProductionEnvironment) {
        return  isProductionEnvironment? getProductionCredentials() : getDevelopCredentials();
    }

    public static String getProductionDataBaseUrl() {
        return "https://lipo-driver-production.firebaseio.com";
    }

    public static String getDevelopDataBaseUrl() {
        return "https://cabbieone-debug.firebaseio.com";
    }

    private static String getProductionCredentials() {
        return "{\n" +
                "  \"type\": \"service_account\",\n" +
                "  \"project_id\": \"lipo-driver-production\",\n" +
                "  \"private_key_id\": \"024db4176058399f4c11bce074b9da72642132fa\",\n" +
                "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsUheBF5cu6gvm\\nZYl+4KqMaKQ8c6GwR4pkT6B7U4/aDXUxBkhQBV+djUNT9ofbUcfaEW0yKep4Gpqg\\nzaoNWTvSW5oYUjM6fcFqMGR3WHPKICw39t8zgZ0Ll2cYyt9OQuo59mONYrAJuAqo\\naJgsXlbpYSpmKU4vMspwzWgnqLIPD2Q/H0QlKveotqkONCqwmeE9vTnPePaLbIri\\n64H+rFQddKkCzZTNzTR7GmQh074NRgoUHVvratxFBaJuFlUXb55TlOC5ijPQkYQo\\nkoeH3/9+UFUt7Mw+oTmJDGKSb3fxRSz8cqTtdCGHD3dyr1YcaTnjXm+UMv4qp1l6\\nUGzUFKk3AgMBAAECggEAAIr/rPX0kX7GS9MNFFvD3c5kmhYpL8OB8Z/4mvrf7Kl3\\nMZDaAHqL2xS7XhPcSLogvZSpeet85CT9UL1lKEacNCT6vtHffHMx/VIqtt+N+diR\\nGxoJiQGDC6rN9duJyZbXYxheJu9lK5qrWe1+cUFY18JpHkpKB0cLMUDMb7eN3BNI\\ngM2Wve19ktVqxqzOjMhYYaxvPoP+Z/CzQKP86QYHCWkaa3forUzggOD/9sXuo2+p\\nwOk1ghqyQcEF+lY9yL4Nc5897m5y15viKsnRLGzqyRLOOjW1KYNrlo1BNwnPhJH6\\n8upNGc2kEDJy9ShEZ9DQei2WaPSFVEvEhS58Se0dQQKBgQDyIy+ixY1jKpD6kjX4\\n2A+jrdAnCo8v2SXlk45/KTKmunCruTofFskR1AauaqQkEQta5+DoyMFhLQl7o8ze\\nxr5HEP/l2ODoZtUPiBacuyILIKrhxqmYXql0rMMh9HPWuex2Ks6Ym/wG7BVcVqwv\\n3bM8/xfNAArdShqKzoSBG7eV9wKBgQC2L6hKksZ9J/+8AcdeXVGhVeNB8KC7AD73\\n8rT3qYr1XeIbkk+3KQCo2AqbV9tpLhBMhilVnSPDraR27LogLPGQsK0Wjl6SoADq\\nzl9zfKsY8usfFYWvkmJCGxYX9pckxepbNb9NRgMd59Lc/fsDbirG+UVJREgCHI5G\\nXbAQZqm2wQKBgDxknksA7wTe5w44TZHlgfEXNyWlspUCmjsqlI4O3n1LXzLVnjOG\\n4Vu7AHecTZhZT/W6hBRLjkAIlabq8fxWobrj+I+BiocsrtOS4+Dvo8wWt8hcXoyM\\nf8OEG59qc0IJWXsXrjPOJAwt8h332DaQG1aJYYxWCF+tTfV1EQOKwD19AoGAc4TG\\nbfqhNuys4DY0hyYew3DlN4NvNwOR6cAQdVUgkjJkU10zhIzQKT9LgnJz4j5eAVe0\\nT9/itIme1fWGky5MQZV1Ou8tEkW4LyqKJX0wNBKbeUs5SDlGamooWSt3bM69qfXr\\nhgDVqxm6f3vftK4fG6klAK72Bbi7keyH3e5qwEECgYBgQiM8MKk54BNW1FM9GKuB\\nGWUoiABGPqrTwAetVgJgNpyM/Z72qkMktr2mIGE/zI12Ce6gkSoVZs4zVaeAiXyP\\nV61w2iEnImc2f7jxhqADSYW/3Vo5Wf2RUt+LH9h+CqxpZ5z8gNT2pd+tXjqxmU1g\\nviyYOc7WYlGh4mr/A/a45w==\\n-----END PRIVATE KEY-----\\n\",\n" +
                "  \"client_email\": \"firebase-adminsdk-c13fh@lipo-driver-production.iam.gserviceaccount.com\",\n" +
                "  \"client_id\": \"101462927123952820703\",\n" +
                "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-c13fh%40lipo-driver-production.iam.gserviceaccount.com\"\n" +
                "}\n";
    }

    private static String getDevelopCredentials() {
        return "{\n" +
                "  \"type\": \"service_account\",\n" +
                "  \"project_id\": \"cabbieone-debug\",\n" +
                "  \"private_key_id\": \"ad642c78e7394006ca5b078d7d1aae7baf12114a\",\n" +
                "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC/pMp4PM+6Bv+5\\nLUU2fdMqjfulXzO0qqeq/6xmyrSjBZWSmA1wHSNT0QQ3hneuo7/L3EOHnmKNigrR\\no4iXcqHBPjYc13niqCyS/EE0U0i5f9DSuwBwYipWKdRkQgvQ5ysdlTLA/hm0aLLn\\nk+VmJFX1tsBIkcPiBOEffWe+9XYp00oWkwYyDrRUgSfbBgQIQZMeha6MsLgORLzz\\nh+lBu0xWgknrR5eMmhQLsYZQtUOOInsCsR5+ttkUqveD1yIv4JosLN1OQF12Xp7R\\nbM8nhx1czfzuw5NeeKpdQRjPCfEmE0sIHrhXAtF8QEaGzgPWEJ0VEKw/Bs65V1Y4\\nIVVqkB6JAgMBAAECggEAEtAZBiOmGrNsMB/3DuncLG3CudG2MDKeggVWRcL5Z5V5\\nQOpHS2241ZYLrW24Wv79KYSGZo5QS2e1Ny4wDvn0UhWK11r5vpvWwFkeB4Zzea5H\\n8B/mY6HnWQ0DKhJnuYqv61gFovduy4yJn30fxF6lx5i4mIcirRHgjahQYmkTP/iv\\n57Y2cH5lt9dKUJ41YO14aK7WMopWdeR+TB+/53rp5CnC0PAuVlIQQ31hlPITYQWb\\n1oJ/kNTTc0nzrPjyIhkJmJH/0xCd4+xYeKNRfdFJ9IUsz6YrLz7fRbgv5C2yPC8P\\nftfgOCzDlv7TdR6PdAFZ5XQm26GTc7bYplTLR+scHwKBgQDkXi5O1F1yLHvu/gUz\\nMUDtNKmCEGtenBPOhUi0GQSKP+9yoCeJZIJP7zWh+RBFoCuJFJXqEshslte3Prbj\\nRfUnGMF5R++gV22oPsKKEyUPcH8wS3ytIRYw2ERYQ3ID8HHyHkPKOjuQWsj8AyLw\\n9TgOg2MqyiLZSokG7EBt2RyDfwKBgQDW1Q8Ez/X4ZJVDDboqbOyABpR0NwMThdme\\n7BECIw0DBnOvVauN2SFQDn3Mkt5CWPcM0xgeL1AfAzDm+v05j13sLXqx3+YZRuH8\\nXP6VVYfirVUNqhoe07crrujU+9KQKU2PCLgCqa50IiVSb1+vedpU4PsRNnrTYSU9\\ne4gGelZB9wKBgGBmhWPPb7bvok1ru6Q1iXPYhCmcZsakWhQW3W7xFKKLhLe9dZMg\\n84VstBYwvja1+0q9ft81mJeGQX9gIPHlqSwc3iclP7lOzsJz48lHdaCD25z2cKXg\\nzq5FzOv5ZJrXGDKdpf4rANPaDvebF1fiHAy8V9NLStSxCxD6D+SO/uNfAoGAcYjZ\\nzhZFjMdDZtN7Xm6AMkzH6f8kvg2P7AAgIPv7JW0keDYTWvy0MUXnT9USZwfzUDDD\\nXzhr5GfgUUG1z9Sns2h5nkWQqi0ewys2VB+Gl9I4z+bktQP4mrpuTBjlt9BML9pQ\\nvP+FtdRR2xYHAIqqSw83QGqFUO7XKn39bkb7DRMCgYEAsZ6J+TASm1IJtMnLMSf7\\njnVs2clXNhfuIrRpkPl01no9930jLXnePOYEYAiTEu+d8+bBrHowOd//CBRjF0rm\\no2YWLbJovR+fW2evf57nqjWQVkkfY/vmyYsx7D906tcjRdXHOmAPZ892if5JbFVF\\nYfBW0P/iKnWMTz1zpfjKYN8=\\n-----END PRIVATE KEY-----\\n\",\n" +
                "  \"client_email\": \"firebase-adminsdk-qzk17@cabbieone-debug.iam.gserviceaccount.com\",\n" +
                "  \"client_id\": \"116764373626857804213\",\n" +
                "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-qzk17%40cabbieone-debug.iam.gserviceaccount.com\"\n" +
                "}\n";
    }
}
