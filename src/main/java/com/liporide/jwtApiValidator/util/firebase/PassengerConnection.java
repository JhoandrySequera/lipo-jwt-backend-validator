package com.liporide.jwtApiValidator.util.firebase;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.liporide.jwtApiValidator.util.FirebaseConection;

import java.io.IOException;
import java.util.logging.Logger;

public class PassengerConnection extends FirebaseConection {

    private final static Logger LOGGER = Logger.getLogger(PassengerConnection.class.getName());

    private static final String PASSENGER_FIREBASE_PROJECT  = "passenger";

    public static FirebaseAuth getAuthReference(boolean isProductionEnvironment) throws IOException {
        LOGGER.info("info by project "+PASSENGER_FIREBASE_PROJECT);
        FirebaseApp passengerFirebaseApp = loadConfiguration(getCredentials(isProductionEnvironment),
                getrDatabaseUrl(isProductionEnvironment), PASSENGER_FIREBASE_PROJECT);
        return FirebaseAuth.getInstance(passengerFirebaseApp);
    }

    private static String getrDatabaseUrl(boolean isProductionEnvironment) {
        return isProductionEnvironment? getProductionDataBaseUrl() : getDevelopDataBaseUrl();
    }

    private static String getCredentials(boolean isProductionEnvironment) {
        return  isProductionEnvironment? getProductionCredentials() : getDevelopCredentials();
    }


    public static String getProductionDataBaseUrl() {
        return "https://lipo-passenger-production.firebaseio.com";
    }

    public static String getDevelopDataBaseUrl() {
        return "https://cabbie-passenger-debug.firebaseio.com";
    }

    private static String getProductionCredentials() {
        return "{\n" +
                "  \"type\": \"service_account\",\n" +
                "  \"project_id\": \"lipo-passenger-production\",\n" +
                "  \"private_key_id\": \"214bd872a88b70be1e1e7cc1f5ac841ae5b2d55f\",\n" +
                "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC0jIOW2DZYkOyE\\na8PTOXIedJD2t+qBxJD9wyO23FnUhN6r2ffVqUUm5mYBpu5FkE15/2r5sb5UsoXW\\nuuYVbOR981R1a2ja4DHTbb5v3DdzyCmJmlAY2RFIXj74VRXaIw18Y8kJYSzk4MF4\\n3VlLNTqLFNVf+dR1zSgz3HX+FEJ6DGZvn8outTHFtItKWGMC91CmP2H+kVUved/g\\nANn1kBYvuedB8Uz/S8XizHu/NaaE5GO3kthZ0rpah/b4DhUKWuoncpN+ePFEHMrw\\nt4GHwSzk9JKoCf3wa8iHuz7+FOBhWv58HpMJeBIZBMjciVkh5GLWWnoIdpXLiHpm\\nr9XGzSnPAgMBAAECggEAGZ9fWrGbH303CPaU4eMN06NN91V+eFAXFjbyrelzmM9f\\nx8UKgPihZwsyKsXanBCwjhcLbXLQxLWufMd6uPZX2Zqwv8Ewg5oRjXnORD2yY5di\\ncwltQzl6/xQw8jkmUsDAEd5vZ19dFFY5NBflq4ta9ZCiqhjgZ/S84dnwUiXkkUFN\\nj5WFE5rvOkr5ZoBshddGN/OXDIXOmAHoJrhrMQw/So3H7zT47ZfXJU2P+di3edtQ\\ndQ0NegT4HFzWU2RPrKrPZO1W2R0HplWMhOUFmuPhaDtl2TlvAw0VNyFiIepo+eBW\\ngo1YYJSd4mL+oUmpTmoihm/Dzk4H67xr4HKDyARysQKBgQDY1BGPWH5SrW7A9vBm\\n6MxUNVmZ1NjKbsbsyDP+r2DX1a1SngE0L51Qv3XmOn5l0rOufgy/tzGuBKGksdV0\\nGfQ27IVjUuAU5mNYW+OyuXM+k9wy4Uft8TxKDzisiqPO6LvjExOynSaEbRaQUZLo\\nONbKvBp6c5+WFJlHWYB798E/CwKBgQDVKpT7lxvM/IDsGqeYGyoV1xys+83aLSbX\\n6vSkQeND9pAeLsRUHvyzIR3iXaqcIukZCc8iADz7fYcfDaYvZj/etVsAGW8croqO\\n8CrranVNPOgOeBMuWkWik9pzrdWoCvstKrrmf739Hq9q8Ty5K5r5EPT6Jd0og057\\no2e3LU3KzQKBgQCeUwuXbfifikAq6qVTn31Dp8kLSJ4cApOCZhWBTLIcJ8xtXS5n\\n2rS2N3dxaxjMZPQK38OTYcMG3B7j0W6ZXjIywLYNdoBlAm1cIYy7/iomcXUCOfl5\\nxwJM2Zx6cKl39fzrrAxgCdwMKlOlgcsRgh9k3cE/1JmczuG1dacZ3ijfTQKBgHmh\\noX7pWyidD6cpEmYdYecyZh9iUrRiPR7I5yzul5IJF3TRdJ8XS1Oe2uF2VspetSjf\\nSvx4rFNH7hjO/ftf2aPnUXR+DsOL6G3lJmhLx4D59uOCqlcxlThOK0EEeLw4/9dm\\n4VxymWLb5nYcDm0OL5Bcl5xAjK/4NIkh1SVVSWUlAoGBAI1xS2nj6d0W+QH4pwXV\\nkIDmLlSdZS9AbQHjFmvsv5UpCZ4fuV8940BOEjHvnx5/uGSErfr0XBrdqzsoVkfd\\nJdqzZ8G37XuD0PgWU6/rYdeL3uHDAvNsi5yeRYDbDAH40HLH7GdhMmM7+im2at+F\\ncTi0QC4KjMKoAMyEDncNIcao\\n-----END PRIVATE KEY-----\\n\",\n" +
                "  \"client_email\": \"firebase-adminsdk-3fcuv@lipo-passenger-production.iam.gserviceaccount.com\",\n" +
                "  \"client_id\": \"112277250981298476840\",\n" +
                "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-3fcuv%40lipo-passenger-production.iam.gserviceaccount.com\"\n" +
                "}";
    }

    private static String getDevelopCredentials() {
        return "{\n" +
                "  \"type\": \"service_account\",\n" +
                "  \"project_id\": \"cabbie-passenger-debug\",\n" +
                "  \"private_key_id\": \"a30ed105406b098fba78fa8cdd0341838c8e74fd\",\n" +
                "  \"private_key\": \"-----BEGIN PRIVATE KEY-----\\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDFLdu46WG0bDFn\\nmXQHtorlxK0BBnWhETZdSkx5VH1nWWMyGhqF1buYMPSEPXf2XFN4WOfB/43fjWqp\\nPgj8AqKUN5osuLdfPOInRo/4zMlRqNemB6hoDyt3hgiVNiTM/etET8RIid12rEyA\\nOXeJfd6uPK+p0Xs3x7oVPbbmB1qU+OEyrZmkFpwH14Klsd/OYqLL5rJ5hP5Ia2sr\\n4Qsk1yl0c1qXmaHP/rFsbm1DjBaGev3bwki37Z1SLoSoeZZ3R2T9AL/W/wZt3BUt\\nJT3oXj9y62E18AZC836qfjhB7trtkX00n5Zmf4fn9a81Z0foSLzCS36aSzy2RSGK\\n5rjOIT1bAgMBAAECggEAQH+TL4Xb6XY4pPBLxZgfEDDI/Jxdzu1/8H00zb1m9e8F\\n5SYnxd6Xqh1dZoJq4sHppeTDDrDm+hjaqni53KGKB9y2SGm1xef2/Cz3PEXUlqsZ\\nu1wm6L8Jk2PbG9/los5ubl5/UNEUCdUrgwgJQ4bSWAFO3shAu4rQIlGZKd5+8Vpy\\nr+WlKtRl8A+adrtCYPw5rd5hFIb6JdgZggRCoiYvPkr0c3vQ1Q8tiXp4ATyNqjwh\\nKcr+9DYpTSao9f/MNwIwtdMXObMCIGLdWgyvDlAStqsKCpg8CNRpI9h3Fs6RMIMw\\n+KHqb/N5u3XJ7kN0ysJ/A3tOO+xtKYeC6lnE3GMK0QKBgQDtNZPHVYrnSmhFxX1f\\nPSFnIHMtqngl4AVJuj5g3BGBICogbmaqDfj4Y8uqvLNijzNG3UfGBBG7RB0sVnOf\\nBZr5KBqB0gwOyJxOekmAUVen8fm9Or7/jJtAj0vSBSJgZL3Xp/IHpmRKS18fImCD\\nHLPCdsrCrpXy3GI1m4dMy2IKsQKBgQDUzIAMLrnvYVDaFAR1oDp7676e5YMGMcNE\\nAfo2wWW1E8fXjyLazQCrmPN3zh0i0rwr4VhLbxdgWj6H8SSsDW+kMLWghdta0QiP\\nmEIAgt9jDYrYDq1TY50CWlloXaeJfZEw/SDUdi0rPwG/Km76LAmaXJNfWhtvylec\\nSqWYQLmzywKBgHUM0g4zrOYytj9vk0zyqk/sGEm5a64s3A8kgvFixpR0HbWBJNHX\\nMTrIa9ClatNAnGnkeS1CBsnkc4zUYBthNbaqzmGvF1oYLeEfwLM78EgkRDrXCTaq\\n2FVEIILTG8J7E56d9fVGawiqJpcDbKJmAGH+IlFsuvtrOl7/jwUa+xcRAoGBAJ80\\nLCxxHOyJ6xps1XVHHdouYluwrBkLS+JqdE9Y24Xguad8z//QVcsL0EL2snemH9/Y\\nZN1yzJNqkNPVxrDuEHNPeeMRoi3Mq9H6gI6vWv9ZV2ITgPsdUI89FxzszHYhumlj\\nR5eMnnh3WBA0Z/jEz55r2loBKPu271rfwgopioWHAoGALD+mcDw3bZ/zd/C7DSOX\\ncVpdzYJfoSMclwV07F7pPlcofO8sXzhtdNYYeRx270TvPho43a6xpynQkF892qHF\\ngMEV0kS7LFvEUXH0CRuhaWtjgud1yYfUL8D6f9t9Tz8IM4KZLTRJVpeca5i5rkmG\\neY+bL3ieDP3HE28bq63XMww=\\n-----END PRIVATE KEY-----\\n\",\n" +
                "  \"client_email\": \"firebase-adminsdk-1uejt@cabbie-passenger-debug.iam.gserviceaccount.com\",\n" +
                "  \"client_id\": \"110719201618880866699\",\n" +
                "  \"auth_uri\": \"https://accounts.google.com/o/oauth2/auth\",\n" +
                "  \"token_uri\": \"https://oauth2.googleapis.com/token\",\n" +
                "  \"auth_provider_x509_cert_url\": \"https://www.googleapis.com/oauth2/v1/certs\",\n" +
                "  \"client_x509_cert_url\": \"https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-1uejt%40cabbie-passenger-debug.iam.gserviceaccount.com\"\n" +
                "}";
    }
}
